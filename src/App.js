import React from "react";
import "./App.css";

import AddPosts from "./components/AddPosts";

function App() {
  return (
    <div className="App">
      <AddPosts />
    </div>
  );
}

export default App;
