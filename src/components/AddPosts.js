import React, { useState } from "react";
import axios from "axios";
function AddPosts() {
  const [formData, setformData] = useState({
    post: "",
    title: "",
  });
  const handleChange = (event) => {
    setformData({ ...formData, [event.target.name]: event.target.value });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post(`https://jsonplaceholder.typicode.com/posts${formData}`)
      .then((respose) => {
        console.log(respose);
      })
      .catch((err) => console.log(err));
  };
  return (
    <div>
      <form action="post" onSubmit={handleSubmit}>
        <input
          type="text"
          name="post"
          value={formData.post}
          onChange={handleChange}
        />
        <input
          type="text"
          name="title"
          value={formData.title}
          onChange={handleChange}
        />
        <input type="submit" value="Save" />
      </form>
    </div>
  );
}
export default AddPosts;
