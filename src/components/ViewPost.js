import React, { useState, useEffect } from "react";
import axios from "axios";
function ViewPost(props) {
  const [post, setPost] = useState({});
  const id = props.match.params.id;
  //OR const { id } = props.match.params;
  useEffect((id) => {
    axios
      .get(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then((response) => {
        console.log(response.data);
        setPost(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  });

  return (
    <div>
      <h2>Post : {post.title}</h2>
    </div>
  );
}

export default ViewPost;
